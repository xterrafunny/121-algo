# Алгоритмы и Структуры Данных
## ФПМИ, МФТИ. Группа Б05-121

### Темы семинаров
#### Семинар 1. Динамическое программирование. Оптимизация на префиксе
#### Семинар 2. Динамическое программирование. Матричное произведение
#### Семинар 3. Динамическое программирование. Оптимизация на подмножествах
#### Семинар 4. Хеш-таблицы
1) `hash_table.cpp` - простая хеш-таблица
#### Семинар 5. Базовые алгоритмы на графах. DFS
1) `subtree_queries.cpp` - подсчет времен входа и выхода через DFS
2) `topsort_dag_dp.cpp` - количество путей от одной вершины до всех остальных на DAG
