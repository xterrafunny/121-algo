#include <iostream>
#include <vector>

using namespace std;

void DFS(int v,
         int p,
         int current_tin,
         int& current_tout,
         vector<int>& tin,
         vector<int>& tout,
         const vector<vector<int>>& graph) {
  tin[v] = current_tin++;
  for (int u : graph[v]) {
    if (u == p) {
      continue;
    }
    DFS(u, v, current_tin, current_tout, tin, tout, graph);
  }
  tout[v] = current_tout++;
}

void Solve() {
  size_t n;
  cin >> n;
  vector<vector<int>> graph(n);

  for (int v = 1; v < n; ++v) {
    int p;
    cin >> p;
    --p;
    graph[p].push_back(v);
  }

  vector<int> tin(n, 0);
  vector<int> tout(n, 0);
  int current_tout = 0;
  DFS(0, -1, 0, current_tout, tin, tout, graph);

}

/*
  1
  |
  2
  |
  3
  |
  4

  1 2 3
*/

int main(int argc, char* argv[]) {
  Solve();
}
