#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

void DFS(int v, vector<char>& used, vector<int>& result, const vector<vector<int>>& graph) {
  used[v] = true;

  for (int u : graph[v]) {
    if (used[u]) {
      continue;
    }

    DFS(u, used, result, graph);
  }

  result.push_back(v);
}

vector<int> Topsort(const vector<vector<int>>& graph) {
  vector<char> used(graph.size(), 0);
  vector<int> result;
  for (int v = 0; v < graph.size(); ++v) {
    if (!used[v]) {
      DFS(v, used, result, graph);
    }
  }
  reverse(result.begin(), result.end());
  return result;
}

void Solve() {
  size_t n, m; // n - количество вершин, m - количество ребер
  cin >> n >> m;
  int s; // s - стартовая вершина
  cin >> s;
  --s;
  vector<vector<int>> graph(n); // Списки смежности
  for (size_t i = 0; i < m; ++i) {
    int u, v;
    cin >> u >> v;
    --u;
    --v;
    graph[u].push_back(v);
  }

  vector<int> topsort = Topsort(graph);

  vector<int> count(n, 0);
  count[s] = 1;
  for (int v : topsort) {
    for (int u : graph[v]) {
      count[u] += count[v];
    }
  }

  for (int i = 0; i < n; ++i) {
    cout << count[i] << ' ';
  }
}

int main(int argc, char* argv[]) {
  Solve();
}

/*
4 5 2
1 2
1 3
2 4
3 4
2 3

0 1 1 2
*/
