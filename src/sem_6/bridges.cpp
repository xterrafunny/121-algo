#include <iostream>
#include <unordered_set>
#include <vector>

template<typename U, typename V>
struct std::hash<std::pair<U, V>> {
  size_t operator()(const std::pair<U, V>& pair) const {
    return std::hash<U>()(pair.first) ^ std::hash<V>()(pair.second);
  }
};

struct Graph {
  Graph(size_t n, size_t m) : n(n), adj(n), tin(n), ret(n), color(n), color_size(n) {
    for (size_t i = 0; i < m; ++i) {
      int u, v;
      std::cin >> u >> v;
      --u;
      --v;
      adj[u].push_back(v);
      adj[v].push_back(u);
    }
  }

  void FindBridges() {
    std::vector<bool> used(n, false);
    int current_time = 0;
    DFS(0, -1, current_time, used);
  }

  void DFS(int v, int p, int& current_time, std::vector<bool>& used) {
    used[v] = true;
    tin[v] = current_time++;
    ret[v] = tin[v];
    for (int u : adj[v]) {
      if (u == p) {
        continue;
      } else if (used[u]) {
        ret[v] = std::min(ret[v], tin[u]);
      } else {
        DFS(u, v, current_time, used);
        ret[v] = std::min(ret[v], ret[u]);

        if (ret[u] > tin[v]) {
          bridges.insert({u, v});
          bridges.insert({v, u});
        }
      }
    }
  }

  int Color() {
    std::vector<bool> used(n, false);
    int current_color = 0;
    for (int v = 0; v < n; ++v) {
      if (used[v]) {
        continue;
      }
      color[v] = current_color++;
      color_size[color[v]] = ColorDFS(v, used);
    }
    return current_color;
  }

  int ColorDFS(int v, std::vector<bool>& used) {
    used[v] = true;
    int size = 1;
    for (int u : adj[v]) {
      if (used[u] || (bridges.find({u, v}) != bridges.end())) {
        continue;
      }
      color[u] = color[v];
      size += ColorDFS(u, used);
    }
    return size;
  }

  size_t n;

  std::vector<std::vector<int>> adj; // списки смежности
  std::vector<int> tin; // время входа
  std::vector<int> ret; // минимальное достижимое время входа из поддерева
  std::vector<int> color; // номер компоненты реберной двусвязности
  std::vector<int> color_size; // размер компонент реберной двусвязности
  std::unordered_set<std::pair<int, int>> bridges; // мосты
};

void Solve() {
  size_t n, m;
  std::cin >> n >> m;

  Graph graph(n, m);
  graph.FindBridges();
  int color_count = graph.Color();

  std::vector<int> color_degree(color_count, 0);
  for (const auto& bridge : graph.bridges) {
    ++color_degree[graph.color[bridge.first]];
  }

  int answer_size = 0;
  int64_t answer_count = 1;
  for (int i = 0; i < color_count; ++i) {
    if (color_degree[i] <= 1) {
      ++answer_size;
      answer_count = (answer_count * graph.color_size[i]) % 1000000007;
    }
  }

  std::cout << answer_size << ' ' << answer_count << '\n';
}

int main(int argc, char* argv[]) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  Solve();
}